package HomeTask2;
// Lab 7
// Написать сериализуемый класс с методом меняющим состояние объекта.
// В мэйне создать объект этого класса, сериализовать, десериализовать, с помощью рефлексии вызвать метод меняющий состояние.
// Покрыть оба класса тестами с помощью джейюнита

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SerializationReflection {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, IllegalArgumentException, ClassNotFoundException, IOException {
        CatFamily2 catfam1 = new CatFamily2();
        catfam1.setFamilyName("Кошка");
        Cats2 cat1 = new Cats2("Мурзик", 1, "белый");
        Cats2 cat2 = new Cats2("Мурзик2", 2, "желтый");

        Serialization(cat1);

        cat1.setWeight(2);

        Deserialization(cat2);


        Class cls = cat2.getClass();
        try {
            cls.getMethod("setWeight", int.class).invoke(cat2, 4);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }


        Field privateIntField;
        try {
            privateIntField = catfam1.getClass().getDeclaredField("x");
            privateIntField.setAccessible(true);
            int x = (Integer) privateIntField.get(catfam1);
            System.out.println("Было поле x = " + x);
            privateIntField.set(catfam1, 25);
            x = (int) privateIntField.get(catfam1);
            System.out.println("После рефлексии x стало = " + x);

        } catch (Exception e) {
            e.printStackTrace();
        }

        receivePrivateFieldsParent(catfam1);

} //end of main

    public static Cats2 Serialization(Cats2 cat){
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("cat1.ser"))) {
            System.out.println("Cat1" + cat);
            os.writeObject(cat);
            os.flush();
            os.close();
        } catch (IOException e) {
            System.out.println("Исключение в момент сериализации" + e);
        }
        return cat;
    }


    public static Cats2 Deserialization(Cats2 cat) throws IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream("cat1.ser"));
        Object obj = (Cats2)is.readObject();
        System.out.println("Восстановленный cat1(теперь cat2): " + obj);
        is.close();
        return cat;
    }

    public static void receivePrivateFieldsParent(CatFamily2 catfam){
        try
        {
            Method m = catfam.getClass().getDeclaredMethod("changeFamName", String.class);
            m.setAccessible(true);
            m.invoke(catfam, " - после рефлексии - это самая обычная кошка");
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

} //end of Serialization

//----------------------------------------------------------------------------------------------------------//
class CatFamily2 {
    public String familyName;
    private int x = 10;

    public String setFamilyName(String familyName) {
        this.familyName = familyName;
        System.out.println("Сейчас familyName имя семейства кошачьих - " + familyName);
        return familyName;
    }

    private String changeFamName(String str) {
        this.familyName = familyName + str;
        System.out.println("Теперь поле familyName - " + this.familyName);
        return this.familyName;
    }
}

//----------------------------------------------------------------------------------------------------------//
class Cats2 extends CatFamily2 implements Serializable {
    public String name;  public int weight;  public String color;

    Cats2(String name, int weight, String color) {
        this.name = name; this.weight = weight; this.color = color;
    }

    public int setWeight(int weight) {
        System.out.println("Теперь вес Тома (cat1) = " + weight);
        return weight;
    }

    public String toString() { return " имеет такие данные:" + " name = " + name + "; weight = " + weight + "; color = " + color; }

}
