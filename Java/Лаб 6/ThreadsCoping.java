//Cкачайте папку ааа со страницы http://www.ex.ua/289698529904
//Разархивируйте ее и поместите в корень диска D://
//Запустите программу

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
class ThreadsCoping {

    public static void main(String args[]) throws IOException {
        NewThread t1 = new NewThread("Параллельный поток");

        System.out.println("Введите текст, для копирования файлов из каталога MyDir в NewDir, содержащих в своем названии этот текст, например file или leo или text:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput = reader.readLine();
        String globPattern = "**" + userInput + "**";
        String dirname = "D:/aaa/MyDir";
        System.out.println("Если файл из каталога MyDir, содержит в своем названии введенный текст, то он был скопирован в отдельную папку директории NewDir: ");
        try {
            Files.walkFileTree(Paths.get(dirname), new MyFileVisitor(globPattern, userInput));
        } catch (IOException exc) {
            System.out.println("Ошибка ввода-вывода");
        }

    }
}

class MyFileVisitor extends SimpleFileVisitor<Path> {
    private PathMatcher pathMatcher;
    String inputStr;

    MyFileVisitor(String globPattern, String userInput) {
        pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + globPattern);
        inputStr = userInput;
    }

    int i = 1;
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(pathMatcher.matches(file)) {
            Path newdir = Paths.get("D:/aaa/NewDir/" + inputStr + Integer.toString(i));
            try {
                if (!Files.exists(newdir))
                    Files.createDirectory(newdir);
            } catch (IOException e) {
                System.err.println(e);
            }

            Path dest = Paths.get(newdir + "/" + file.getFileName());
            Files.copy(file, dest, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(newdir+ "\\" + file.getFileName());
            i++;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return FileVisitResult.CONTINUE;
    }
}

class NewThread implements Runnable {
    String threadName;
    Thread tr;

    NewThread(String name) {
        threadName = name;
        tr = new Thread(this);
        System.out.println("Создан параллельный поток: " + tr);
        tr.start();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            System.out.println("Введите текст, для копирования файлов из каталога MyDir в NewDir, содержащих в своем названии этот текст, например file или leo или text:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String userInput = reader.readLine();
            String globPattern = "**" + userInput + "**";
            String dirname = "D:/aaa/MyDir";
            System.out.println("Если файл из каталога MyDir, содержит в своем названии введенный текст, то он был скопирован в отдельную папку директории NewDir: ");
            try {
                Files.walkFileTree(Paths.get(dirname), new MyFileVisitor(globPattern, userInput));
            } catch (IOException exc) {
                System.out.println("Ошибка ввода-вывода");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(threadName + " был завершен");
        }
    }
