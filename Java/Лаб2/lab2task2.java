/**
 *
 2) Переведите значение каждого примитивного типа в строку и обратно выведите
 результаты на экран
 */

public class lab2task2 {
    public static void main (String[] args) {

        byte b = 15;
        String sb = Byte.toString(b);
        System.out.println("число типа Byte переведено в тип String: " + sb);
        b = Byte.parseByte(sb);
        System.out.println("число типа String переведено обратно в тип Byte: " + b);

        short sh = 39;
        String ssh = Short.toString(sh);
        System.out.println("число типа Short переведено в тип String: " + ssh);
        sh = Short.parseShort(ssh);
        System.out.println("число типа String переведено обратно в тип Short: " + sh);

        //!
        int i = 55;
        String si = Integer.toString(i);
        System.out.println("число типа int переведено в тип String: " + si);
        i = Integer.parseInt(si);
        System.out.println("число типа String переведено обратно в тип int: " + i);

        //!
        long l = 234L;
        String sl = Long.toString(l);
        System.out.println("число типа Long переведено в тип String: " + sl);
        l = Long.parseLong(sl);
        System.out.println("число типа String переведено обратно в тип Long: " + l);

        //!
        float f = 25.3f;
        String sf = Float.toString(f);
        System.out.println("число типа Float переведено в тип String: " + sf);
        f = Float.parseFloat(sf);
        System.out.println("число типа String переведено обратно в тип Float: " + f);

        //!
        double d = 32.45d;
        String sd = Double.toString(d);
        System.out.println("число типа Double переведено в тип String: " + sd);
        d = Double.parseDouble(sd);
        System.out.println("число типа String переведено обратно в тип Double: " + d);


        boolean bl = true;
        String sbl = Boolean.toString(bl);
        System.out.println("число типа Boolean переведено в тип String: " + sbl);
        bl = Boolean.parseBoolean(sbl);
        System.out.println("число типа String переведено обратно в тип Boolean: " + bl);

        //!
        char ch = 'a';
        String sch = Character.toString(ch);
        System.out.println("число типа Char переведено в тип String: " + sch);
        ch = "new_string".charAt(0);
        System.out.println("число типа String переведено обратно в тип Char: " + ch);
    }
}