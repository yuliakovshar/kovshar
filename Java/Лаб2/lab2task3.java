/**
 3) Переведите число с точкой в тип без точки и наоборот ­ выведите результаты
 */

public class lab2task3 {
    public static void main (String[] args) {
        float f1 = 343.24f;
        System.out.println("f = " + f1);
        int i = (int)f1;
        System.out.println("Число, переведенное из float в int - " + i);
        float f2 = (float)i;
        System.out.println("Число, переведенное из int в float - " + f2);

        double d1 = 123.754;
        System.out.println("d = " + d1);
        int j = (int)d1;
        System.out.println("Число, переведенное из double в int - " + j);
        double d2 = (double)j;
        System.out.println("Число, переведенное из int в float - " + d2);

    }
}
