/**
 *
 1) Напишите примеры использования данных операторов и выведите
 результаты на экран
 a. Арифметические
 b. Сравнения
 с. Логические
 d. Приравнивания

 */
public class lab2task1 {
    public static void main (String[] args) {
        //a. Арифметические
        int a = 12;
        int b = 3;
        System.out.println("Сумма чисел: " + (a + b));
        System.out.println("Деление чисел: " + (a / b));
        System.out.println("Разность чисел: " + (a - b));
        System.out.println("Умножение чисел: " + (a * b));
        System.out.println("Остаток от деления: " + a%b);

        //b. Сравнения
        if (a > b) {
            System.out.println("a больше b");
        } else System.out.println("b больше a");

        //с. Логические
        if ((a != 11) && (b > 3)) {
            System.out.println("a не равно 11 и b>3");
        } else if ((a == 8) || (b < 5)) {
            System.out.println("a==8 или b<5");
        } else System.out.println("Выполнился этот блок");

        //d. Приравнивания
        int c = 70;
        System.out.println("Результат присвоения: c=" + c);
        int d = ++c + a--;
        System.out.println("Результат присвоение: d=" + d);
        System.out.println("a = " + a);
    }
}
