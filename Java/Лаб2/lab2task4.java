import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*****
 Написать программу которая бы запросила пользователя ввести число, затем
 знак математической операции, затем еще число и вывела результат. Должно работать и
 для целых и для дробных чисел

 */

public class lab2task4 {
    public static void main (String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите первое число: ");
        float a = Float.parseFloat(reader.readLine());
        System.out.print("Введите арифметический оператор: ");
        String str = reader.readLine();
        char c = str.charAt(0);
        System.out.print("Введите второе число: ");
        float b = Float.parseFloat(reader.readLine());
        switch (c) {
            case '+': {
                System.out.println("Результат суммы чисел: " + (a + b));
                break;
            }

            case '-': {
                System.out.println("Результат разницы чисел: " + (a - b));
                break;
            }

            case '*': {
                System.out.println("Результат произведения чисел: " + (a * b));
                break;
            }

            case '/': {
                System.out.println("Результат деления чисел: " + (a / b));
                break;
            }

            case '%': {
                System.out.println("Остаток от деления чисел: " + (a % b));
                break;
            }
            default: {
                System.out.println("Стоп");
                break;
            }
        }
    }
}
