public class lab4 {

    public static void main(String[] args) {
        Car[] cars = new Car[6];
        cars[0] = new Car1(100, 5, 0.2);
        cars[1] = new Car2(200, 4, 0.4);
        cars[2] = new Car3(180, 5, 0.3);
        cars[3] = new Car1(90, 3, 0.5);
        cars[4] = new Car2(140, 2, 0.7);
        cars[5] = new Car3(170, 3, 0.7);
        for (Car y: cars){
            for (int i=0; i<20; i++) {
                y.drive();
                y.turn();
            }
            y.minsec();
        }

        for (int i=0; i<cars.length; i++){
            System.out.println("Автомобиль " + (i+1) + " прошел трассу за " + cars[i].minute + " минут " + cars[i].second +" секунд");
        }

    }

    protected static class Car {
        private double reachedSpeed;
        private double accel;
        private double manevr;
        public double drivetime;
        public double initspeed = 0;
        public int totaltime = 0;
        int minute = 0;
        int second = 0;

        Car(double reachedSpeed, double accel, double manevr) {
            this.reachedSpeed = reachedSpeed;
            this.accel = accel;
            this.manevr = manevr;
        }

        public double drive() {
            drivetime = ((reachedSpeed/3.6) - initspeed) / accel;
            return drivetime;
        }

        public double turn(){  return 0; }

        public int minsec () {
            minute = totaltime/60;
            second = totaltime - minute*60;
            return minute;
        }

    }

    //едет машина класса Car1
    public static class Car1 extends Car {
        final int maxSpeed = 190;
        public Car1(double reachedSpeed, double accel, double manevr) {
            super(reachedSpeed, accel, manevr); // вызов конструктора суперкласса
        }

        //TURN FOR 1
        @Override
        public double turn(){
            //если скорость автомобиля на момент поворота больше половины от максимальной -
            // то он получает +0,5% от разницы между половиной от максимальной и нынешней к маневренности на этот поворот
            if (super.reachedSpeed>(maxSpeed/2)){
                double profit = (0.5 * (super.reachedSpeed - (maxSpeed/2)))/100;
                super.manevr += profit;
            }

            double turnspeed = (super.reachedSpeed/3.6) * super.manevr;
            initspeed = turnspeed;
            double turntime = ((super.reachedSpeed/3.6) - turnspeed)/super.accel;
            totaltime += (drivetime + turntime);
            return totaltime;
        }
    } //end of Car1


    //едет машина класса Car2
    public static class Car2 extends Car {
        final int maxSpeed = 220;
        public Car2(double reachedSpeed, double accel, double manevr) {
            super(reachedSpeed, accel, manevr);
        }

        //TURN FOR 2
        @Override
        public double turn(){
            double turnspeed = (super.reachedSpeed/3.6) * super.manevr;
            initspeed = turnspeed;
            double turntime = ((super.reachedSpeed/3.6) - turnspeed)/super.accel;
            totaltime += (drivetime + turntime);
            if (super.reachedSpeed<(maxSpeed/2)){
                super.accel *= 2;
            }
            return totaltime;
        }
    } //end of Car2

    //едет машина класса Car3
    public static class Car3 extends Car {
        public int maxSpeed = 220;
        public Car3(double reachedSpeed, double accel, double manevr) {
            super(reachedSpeed, accel, manevr);
        }

        //TURN FOR 3
        @Override
        public double turn(){
            if (super.reachedSpeed==maxSpeed){
                maxSpeed += (maxSpeed/100) *10; //максимальная скорость увеличивается на 10% до конца гонки;
            }
            double turnspeed = (super.reachedSpeed/3.6) * super.manevr;
            initspeed = turnspeed;
            double turntime = ((super.reachedSpeed/3.6) - turnspeed)/super.accel;
            totaltime += (drivetime + turntime);
            return totaltime;
        }
    } //end of Car3

}
