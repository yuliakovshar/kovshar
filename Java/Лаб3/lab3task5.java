import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*****
 5) Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
 */

public class lab3task5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        String str = "";

        do {
            str = reader.readLine();
            if (isInteger(str)){
                int a = Integer.parseInt(str);
                sum += a;
            }

        } while (!str.equals("сумма"));

        System.out.println(sum);

    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}





