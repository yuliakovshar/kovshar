/**
 1) Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль.
 */

public class lab3task1 {
    public static void main (String[] args) {
        int[] a = new int[4];
        a[0] = 2;
        a[1] = 10;
        a[2] = 6;
        a[3] = 4;


        if (a[0]<a[1] && a[0]<a[2]&&a[0]<a[3]) System.out.println("Минимум, найденный с помощью условия: " + a[0]);
        if (a[1]<a[0] && a[1]<a[2]&&a[1]<a[3]) System.out.println("Минимум, найденный с помощью условия: " + a[1]);
        if (a[2]<a[0] && a[2]<a[1]&&a[2]<a[3]) System.out.println("Минимум, найденный с помощью условия: " + a[2]);
        if (a[3]<a[0] && a[3]<a[1]&&a[3]<a[2]) System.out.println("Минимум, найденный с помощью условия: " + a[3]);



        int min=a[0];
        for (int i = 1; i < a.length; i++) {
            if (min > a[i]) {
                    min = a[i];
            }
        }
        System.out.println("Минимум, найденный с помощью цикла: " + min);
    }
}
