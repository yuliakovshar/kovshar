/**
 2) При помощи цикла for вывести на экран нечетные числа от 1 до 99.
 */

public class lab3task2 {
    public static void main (String[] args) {
        for (int i=1; i<99; i=i+2) System.out.println(i);
    }
}
