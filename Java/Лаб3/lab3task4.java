/**
 4) Дан массив из 4 чисел типа int. Вывести их все.
 */

public class lab3task4 {
    public static void main (String[] args) {
        int[] arr = new int[4];
        arr[0] = 23;
        arr[1] = 56;
        arr[2] = 6;
        arr[3] = 4;
        for (int b: arr){
            System.out.println(b);
        }
    }
}
