import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
//
////*** 3) Вывести все простые числа от 1 до N. N задается пользователем через консоль
//

public class lab3task3 {
    public static void main (String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите число больше 1: ");
        int N = Integer.parseInt(reader.readLine());


        if (N!=1) {
            for (int m = 2; m <= N; m++) {
                if (m == 2) System.out.println(2);
                else if (m == 3) System.out.println(3);
                else {
                    for (int i = 2; i * i <= m; i++) {
                        if (m % i == 0) {
                            break;
                        } else System.out.println(m);
                        break;
                    }
                }
            }
        }

    }
}
