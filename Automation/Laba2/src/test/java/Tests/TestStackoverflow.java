//////////////////   LABA 2 TESTSTACKOVERFLOW  //////////////////////////

package Tests;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;


public class TestStackoverflow {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @AfterClass
    public static void tearDown(){
        driver.close();
    }


    @After
    public void afterEachTest(){
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void Test_a() {
        WebElement stack_featured_number = driver.findElement(By.xpath(".//*[@id='tabs']/a/span"));
        String stack_str = stack_featured_number.getText();
        int stack_int = Integer.parseInt(stack_str);
        Assert.assertTrue("The count is less than 300", stack_int>300);
    }

    @Test
    public void Test_b() {
        WebElement stack_sighup_link = driver.findElement(By.xpath(".//a[@class='login-link'][contains(@href, 'signup')]"));
        stack_sighup_link.click();
        WebElement stack_button_google = driver.findElement(By.xpath(".//div[@id='openid-buttons']/div/div/span[contains(text(),'Google')]"));
        WebElement stack_button_facebook = driver.findElement(By.xpath(".//div[@id='openid-buttons']/div/div/span[contains(text(),'Facebook')]"));
        assertNotNull("The Google button isn't present!", stack_button_google);
        assertNotNull("The Facebook button isn't present!", stack_button_facebook);
    }

    @Test
    public void Test_c() {
        WebElement any_top = driver.findElement(By.xpath(".//*[@class='summary']/h3"));
        any_top.click();
        WebElement today_question = driver.findElement(By.xpath(".//table[@id='qinfo']/tbody/tr[1]/td/p/b"));
        String asked_str = today_question.getText();
        assertEquals("Question wasn't asked today!", "today", asked_str);
        //WebElement today_question = driver.findElement(By.xpath(".//div[@id='sidebar']/div/table[@id='qinfo']/tbody/tr[1]/td/p[contains(b,'today')]"));
        //Assert.assertTrue("Question hasn't today label", today_question.isDisplayed());


    }

    @Test
    public void Test_d() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement hot_vacancy = driver.findElement(By.xpath(".//*[@id='hireme']/div/ul[@class='jobs']/li/a/div[@class='title']"));
        String hot_str = hot_vacancy.getText();
        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(hot_str);
        String res="    ";
        while (m.find()) {
            res = m.group();
            System.out.println(res);
        }
        int res_int = Integer.parseInt(res);
        Assert.assertTrue("There isn't such suggestion - more than 100k!", res_int>100);

    }
}

