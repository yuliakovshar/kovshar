//////////////////   LABA 2 TESTROZETKA //////////////////////////
package Tests;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import static junit.framework.TestCase.assertNotNull;


public class TestRozetka {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @AfterClass
     public static void tearDown(){
        driver.close();
    }

    @Test
    public void Test_a() {
        WebElement rozetkalogo = driver.findElement(By.xpath(".//div[@class='logo']/img"));
        //assertNotNull("Logo isn't present!", rozetkalogo);
        Assert.assertTrue("Item 'Apple' isn't presented in catalog menu!", rozetkalogo.isDisplayed());
    }

    @Test
    public void Test_b() {
        WebElement rozetka_menu_apple = driver.findElement(By.xpath(".//*[@class='m-main']/ul//a[contains(text(), 'Apple')]"));
        assertNotNull("Item 'Apple' isn't presented in catalog menu!", rozetka_menu_apple);
    }


    @Test
    public void Test_c() {
        WebElement rozetka_menu_mp3 = driver.findElement(By.xpath(".//*[@class='m-main']/ul//a[contains(text(), 'MP3')]"));
        assertNotNull("Item 'MP3' isn't presented in catalog menu!", rozetka_menu_mp3);
    }

    @Test
    public void Test_d() {
        WebElement rozetka_city_choose = driver.findElement(By.xpath(".//*[@id='city-chooser']/a "));
        rozetka_city_choose.click();
        //WebElement rozetka_city_check = driver.findElement(By.xpath(".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix'][contains(text(), 'Киев') or contains(text(), 'Харьков') or contains(text(), 'Одесса')]"));
        //Assert.assertTrue("Links on city aren't present!", rozetka_city_check.isDisplayed());
        WebElement rozetka_kuiv_check = driver.findElement(By.xpath(".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Киев')]"));
        Assert.assertTrue("Links on city aren't present!", rozetka_kuiv_check.isDisplayed());
        WebElement rozetka_kharkov_check = driver.findElement(By.xpath(".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Харьков')]"));
        Assert.assertTrue("Links on city aren't present!", rozetka_kharkov_check.isDisplayed());
        WebElement rozetka_odessa_check = driver.findElement(By.xpath(".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Одесса')]"));
        Assert.assertTrue("Links on city aren't present!", rozetka_odessa_check.isDisplayed());
    }

    @Test
    public void Test_e() {
        WebElement rozetka_cart = driver.findElement(By.xpath(".//div[@name='splash-button']/div[contains(a,'Корзина')]"));
        rozetka_cart.click();
        WebElement rozetka_title_empty_cart = driver.findElement(By.xpath(".//*[@id='drop-block']/h2[text()='Корзина пуста']"));
        Assert.assertTrue("The cart aren't empty!", rozetka_title_empty_cart.isDisplayed());
    }

}
