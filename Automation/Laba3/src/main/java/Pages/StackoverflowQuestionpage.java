//Laba3
package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowQuestionpage {
    private WebDriver driver;

    public StackoverflowQuestionpage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//table[@id='qinfo']/tbody/tr[1]/td/p/b")
    public WebElement today_question;

}
