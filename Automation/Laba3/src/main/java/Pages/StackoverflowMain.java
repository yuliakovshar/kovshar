//Laba3
package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowMain {
    private WebDriver driver;

    public StackoverflowMain (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='tabs']/a/span")
    public WebElement stack_featured_number;

    @FindBy(xpath = ".//a[@class='login-link'][contains(@href, 'signup')]")
    public WebElement stack_sighup_link;

    @FindBy(xpath = ".//div[@id='openid-buttons']/div/div/span[contains(text(),'Google')]")
    public WebElement stack_button_google;

    @FindBy(xpath = ".//div[@id='openid-buttons']/div/div/span[contains(text(),'Facebook')]")
    public WebElement stack_button_facebook;

    @FindBy(xpath = ".//*[@class='summary']/h3")
    public WebElement any_top;

    public StackoverflowQuestionpage clickOnAnyQuestion() throws InterruptedException{
        any_top.click();
        return new StackoverflowQuestionpage(driver);
    }

    @FindBy(xpath = ".//*[@id='hireme']/div/ul[@class='jobs']/li/a/div[@class='title']")
    public WebElement hot_vacancy;
}
