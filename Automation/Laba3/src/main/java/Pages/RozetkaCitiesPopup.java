//Laba3
package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaCitiesPopup {
    private WebDriver driver;

    public RozetkaCitiesPopup (WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Киев')]")
    public WebElement rozetka_kuiv_check;

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Харьков')]")
    public WebElement rozetka_kharkov_check;

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[@name='content']/div/div[@class='clearfix']//*[contains(text(), 'Одесса')]")
    public WebElement rozetka_odessa_check;

}
