//Laba3
package Tests;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import Pages.*;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

public class TestingRozetka {
    private static WebDriver driver;
    private static Pages.RozetkaMain rozetkaMain;
    private static Pages.RozetkaCitiesPopup rozetkaCitiesPopup;
    private static Pages.RozetkaCart rozetkaCart;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        rozetkaMain = new RozetkaMain(driver);
    }

    @AfterClass
    public static void tearDown(){
        driver.close();
    }

    @Test
    public void checkLogoIsDisplayed(){ //test_a
        Assert.assertTrue("Item 'Apple' isn't presented in catalog menu!", rozetkaMain.rozetkalogo.isDisplayed());
    }


    @Test
    public void checkItemAppleNotNull() {  //test_b
        assertNotNull("Item 'Apple' isn't presented in catalog menu!", rozetkaMain.rozetka_menu_apple);
    }

    @Test
    public void checkItemMp3NotNull() { //test_c
        assertNotNull("Item 'MP3' isn't presented in catalog menu!", rozetkaMain.rozetka_menu_mp3);
    }

    @Test
    public void checkKuivKharkovOdessaDisplayed() throws InterruptedException { //test_d
        rozetkaCitiesPopup = rozetkaMain.clickOnCitiesChooserLink();
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_kuiv_check.isDisplayed());
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_kharkov_check.isDisplayed());
        Assert.assertTrue("Links on city aren't present!", rozetkaCitiesPopup.rozetka_odessa_check.isDisplayed());
    }

    @Test
    public void checkCartIsEmpty() throws InterruptedException { //test_e
        rozetkaCart = rozetkaMain.clickOnCartLink();
        Assert.assertTrue("The cart aren't empty!", rozetkaCart.rozetka_title_empty_cart.isDisplayed());
    }

}

