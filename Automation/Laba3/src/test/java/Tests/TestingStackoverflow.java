//Laba3
package Tests;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import Pages.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestingStackoverflow {
    private static WebDriver driver;
    private static StackoverflowMain stackoverflowmain;
    private static StackoverflowQuestionpage stackoverflowQuestionpage;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        stackoverflowmain = new StackoverflowMain(driver);

    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

    @After
    public void afterEachTest() {
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void checkFeaturedPostMore300() { //test_a
        String stack_str = stackoverflowmain.stack_featured_number.getText();
        int stack_int = Integer.parseInt(stack_str);
        Assert.assertTrue("The count is less than 300", stack_int > 300);
    }

    @Test
    public void checkGoogleFacebookButtonsPresence() { //test_b
        stackoverflowmain.stack_sighup_link.click();
        assertNotNull("The Google button isn't present!", stackoverflowmain.stack_button_google);
        assertNotNull("The Facebook button isn't present!", stackoverflowmain.stack_button_facebook);
    }

    @Test
    public void checkAnyQuestionAskedToday() throws InterruptedException { //test_c
        stackoverflowQuestionpage = stackoverflowmain.clickOnAnyQuestion();
        String asked_str = stackoverflowQuestionpage.today_question.getText();
        assertEquals("The question wasn't asked today!", "today", asked_str);
    }


    @Test
    public void checkSalaryMore100K() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String hot_str = stackoverflowmain.hot_vacancy.getText();
        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(hot_str);
        String res = "str";
        while (m.find()) {
            res = m.group();
            System.out.println(res);
        }
        int res_int = Integer.parseInt(res);
        Assert.assertTrue("There isn't such suggestion - more than 100k!", res_int > 100);

    }

}

